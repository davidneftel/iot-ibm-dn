# iot-device-python

This exercise allows you to send simulated data (x,y,z) from a simple Python program to a device created with the IBM Watson Internet of Things platform under your IBM Cloud account.

## Table of contents

1. [Installation](#Installation)
1. [Usage](#Usage)
1. [Contributing](#Contributing)
1. [Authors and acknowledgment](#Authors-and-acknowledgment)
1. [License](#License)

## Installation

### Docker

The python code is meant to be used inside a Docker container.
First download [Docker Desktop](https://www.docker.com/products/docker-desktop) on your machine. Docker is available for Windows, Mac and Linux.
Once you have downloaded Docker desktop, make sure you have the 'docker' and 'docker-compose' CLI installed.
In the new Docker Desktop versions, the 'docker compose' (without '-') CLI is pre-installed and can be used for all the 'docker-compose' commands.

### Vanilla Python

Prerequisite: Install Python on your computer (PC, MAC)
The latest downloadable version is 3.9.0
<https://www.python.org/downloads/>

Prerequisite: Install the pip extension
Normally this extension is installed with Python. If this is not the case: <https://pip.pypa.io/en/latest/installing/>

To learn more about Python and pip installations:
<https://github.com/BurntSushi/nfldb/wiki/Python-&-pip-Windows-installation>

Verify that Python and pip are properly installed by running a command prompt.
Type under the command prompt of your machine the commands: python -V and pip -V (versions 3.9 and 20.0.2)

Normally python is installed under the directory c:\Python39 (on Windows 10 PC) and the PATH variable has been updated (on Windows). In any case when installing Python, you can choose a directory of your choice.

Under the command prompt, make the following updates and install the wiotp-sdk library that allows access from python to the Watson IoT platform:

```bash
pip install --upgrade pip
pip install wiotp-sdk
pip install --upgrade wiotp-sdk
```

From GitHub, download the source code of the example (ZIP) on your machine in a directory of your choice, unzip the content and copy the directory 'iot-device-python-master' under c:\python39\ (Windows)

You obtain for example :

```bash
C:\Python39\iot-device-python-master
```

## Usage

Edit the file config.json and add your device configuration data created under the Watson IoT platform.

It is necessary to have previously created a device type 'LabPythonDevice' and a device 'IoTDevice1' in an instance of the Watson Internet of Things platform before being able to run the main program.

Replace in the config.json file the connection information of the device with the one you got during the previous creation of the device type and instance (Example below):

```json
{
    "org_id": "uz6g30",
    "domain": "internetofthings.ibmcloud.com",
    "device_type": "LabPythonDevice",
    "device_id": "IoTDevice1",
    "dev_auth_token": "430FEYckMPx5Ovw7mr",
    "qos": 2,
    "disconnect_after": 20,
    "throttleInterval": 1000
}
```

### Usage: Docker

Make sure your internet connection is working. Move to the 'wiot' folder:

```bash
cd wiot
```

Run the following command:

```bash
docker-compose up
```

Or with the newer Docker CLI (I advise to use this one if you can):

```bash
docker compose up
```

### Usage: Vanilla Python

Save the modifications of the file then launch the following command under the c:\Python38\iot-device-python-master directory in order to check the sending of messages and their reception in the platform : python3 send_xyz.py

```python
c:\Python39\iot-device-python-master>python3 send_xyz.py

Wait 20 s (or press Ctrl+C to disconnect)
2020-04-20 23:00:04,256 wiotp.sdk.device.client.DeviceClient INFO Connected successfully: d:uz6g30:LabPythonDevice:IoTDevice1
Event 'a' {'x': -2.109541844739039, 'y': 1.263037762777719, 'z': 0.6850603004111173} sent to WIoTP

Event 'o' {'g': 10, 'b': 8, 'a': 2} sent to WIoTP

Confirmed event at 0 ms, received by WIoTP

Confirmed event at 0 ms, received by WIoTP

Event 'a' {'x': 0.4729797329721448, 'y': -0.019858572463314524, 'z': -1.9682053134574264} sent to WIoTP

Event 'o' {'g': 10, 'b': 8, 'a': 2} sent to WIoTP

Confirmed event at 1000 ms, received by WIoTP

Confirmed event at 1000 ms, received by WIoTP

Event 'a' {'x': -0.709192389465099, 'y': 0.9815754943629975, 'z': 0.24810549122793277} sent to WIoTP

Event 'o' {'g': 10, 'b': 8, 'a': 2} sent to WIoTP

Confirmed event at 2000 ms, received by WIoTP

Event 'a' {'x': 1.9435167237400273, 'y': 1.1500987851196574, 'z': 0.3156225605864271} sent to WIoTP

Event 'o' {'g': 10, 'b': 8, 'a': 2} sent to WIoTP

Confirmed event at 3000 ms, received by WIoTP

Confirmed event at 3000 ms, received by WIoTP

Confirmed event at 3000 ms, received by WIoTP

Event 'a' {'x': 0.6511059401626016, 'y': 1.7393710549308494, 'z': 0.9541822325687821} sent to WIoTP

Event 'o' {'g': 10, 'b': 8, 'a': 2} sent to WIoTP

Confirmed event at 4000 ms, received by WIoTP

Confirmed event at 4000 ms, received by WIoTP
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors and acknowledgment

Made by Clément Stauner, 2021.

## License

[© MIT](https://mit-license.org/)
